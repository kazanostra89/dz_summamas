﻿using System;
using static System.Console;

namespace SummaMas
{
    class Program
    {
        static void Main()
        {
            int R1, R2, R3;
            int a, b;
            int ostatok, summProm;
            bool proverkaVvoda;
            int[] summaMas;

            ostatok = 0;

            do
            {
                Write("Введите размерность первого числа: ");
                proverkaVvoda = int.TryParse(ReadLine(), out R1);
            } while (proverkaVvoda == false);

            int[] chislo1Mas = new int[R1];

            WriteLine("Введите первое число последовательно по одному знаку: ");

            for (int i = 0; i < R1; i++)
            {
                do
                {
                    do
                    {
                        proverkaVvoda = int.TryParse(ReadLine(), out chislo1Mas[i]);
                    } while (proverkaVvoda == false);

                } while (chislo1Mas[i] <= -1 || chislo1Mas[i] >= 10);
            }

            do
            {
                Write("\nВведите размерность второго числа: ");
                proverkaVvoda = int.TryParse(ReadLine(), out R2);
            } while (proverkaVvoda == false);

            int[] chislo2Mas = new int[R2];

            WriteLine("Введите второе число последовательно по одному знаку: ");

            for (int i = 0; i < R2; i++)
            {
                do
                {
                    do
                    {
                        proverkaVvoda = int.TryParse(ReadLine(), out chislo2Mas[i]);
                    } while (proverkaVvoda == false);

                } while (chislo2Mas[i] <= -1 || chislo2Mas[i] >= 10);
            }

            if (R1 == R2)
            {
                R3 = R2;

                summaMas = new int[R3];

                for (int i = (R3 - 1); i >= 0; i--)
                {
                    summProm = chislo1Mas[i] + chislo2Mas[i] + ostatok;

                    if (summProm >= 10)
                    {
                        summaMas[i] = summProm % 10;
                        ostatok = summProm / 10;
                    }
                    else
                    {
                        summaMas[i] = summProm;
                        ostatok = 0;
                    }
                }

                WriteLine();

                for (int v = 0; v < R3; v++)
                {
                    Write(summaMas[v]);
                }
            }
            else if (R1 > R2)
            {
                R3 = R1;

                summaMas = new int[R3];

                for (a = R1 - 1, b = R2 - 1; a >= 0; a--, b--)
                {
                    if (b >= 0)
                    {
                        summProm = chislo1Mas[a] + chislo2Mas[b] + ostatok;

                        if (summProm >= 10)
                        {
                            summaMas[a] = summProm % 10;
                            ostatok = summProm / 10;
                        }
                        else
                        {
                            summaMas[a] = summProm;
                            ostatok = 0;
                        }
                    }
                    else if (a >= 0)
                    {
                        summProm = chislo1Mas[a] + ostatok;

                        if (summProm >= 10)
                        {
                            summaMas[a] = summProm % 10;
                            ostatok = summProm / 10;
                        }
                        else
                        {
                            summaMas[a] = summProm;
                            ostatok = 0;
                        }
                    }
                }

                WriteLine();

                for (int v = 0; v < R3; v++)
                {
                    Write(summaMas[v]);
                }
            }
            else if (R2 > R1)
            {
                R3 = R2;

                summaMas = new int[R3];

                for (a = R1 - 1, b = R2 - 1; b >= 0; a--, b--)
                {
                    if (a >= 0)
                    {
                        summProm = chislo1Mas[a] + chislo2Mas[b] + ostatok;

                        if (summProm >= 10)
                        {
                            summaMas[b] = summProm % 10;
                            ostatok = summProm / 10;
                        }
                        else
                        {
                            summaMas[b] = summProm;
                            ostatok = 0;
                        }
                    }
                    else if (b >= 0)
                    {
                        summProm = chislo2Mas[b] + ostatok;

                        if (summProm >= 10)
                        {
                            summaMas[b] = summProm % 10;
                            ostatok = summProm / 10;
                        }
                        else
                        {
                            summaMas[b] = summProm;
                            ostatok = 0;
                        }
                    }
                }

                WriteLine();

                for (int v = 0; v < R3; v++)
                {
                    Write(summaMas[v]);
                }
            }

            ReadKey();
        }
    }
}
